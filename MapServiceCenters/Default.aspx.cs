﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using System.Data.SqlClient;

namespace MapServiceCenters
{
    public partial class Default : System.Web.UI.Page
    {
        private static string TOOLTIP_TEMPLATE = @"
                <div class=""leftCol"">
                    <div class=""flag flag-{0}""></div>
                </div>
                <div class=""rightCol"" onclick=""parent.open('https://www.google.com/search?q='+ '{4}' +' {2}', '_blank')"" style=""cursor: pointer;"">
                    <div class=""dealerName"">{1}</div>
                    <div class=""city"">{2}</div>
                    <div class=""address"">{3}</div>
                </div>";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //RadMap1.DataSource = GetData();
                //RadMap1.DataBind();
            }
        }

        private DataSet GetData()
        {
            string path = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E";
            DataSet ds = new DataSet("ServiceCenterLocations");

            DataTable dt = new DataTable("ServiceCenterLocationsTable");
            dt.Columns.Add("Shape", Type.GetType("System.String"));
            dt.Columns.Add("Country", Type.GetType("System.String"));
            dt.Columns.Add("ServiceCenterName", Type.GetType("System.String"));
            dt.Columns.Add("City", Type.GetType("System.String"));
            dt.Columns.Add("Address", Type.GetType("System.String"));
            dt.Columns.Add("Latitude", Type.GetType("System.Decimal"));
            dt.Columns.Add("Longitude", Type.GetType("System.Decimal"));

            string sSQL = "select * from veritas.dbo.ServiceCenter where Latitude is not NULL AND Longitude is not NULL " +
                          " ";  //"AND DealerNo not like '%CC' AND DealerName not like '%terminated%' AND active = 1";
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            DataTable currentServiceCenters = dBO.dboInportInformation(sSQL);
            dBO.dboClose();
            
            foreach (DataRow dr in currentServiceCenters.Rows)
            {
                dt.Rows.Add("customColorShape", "United States", dr["ServiceCenterName"].ToString(), dr["City"].ToString(), dr["Addr1"].ToString()+" "+dr["Zip"].ToString(), decimal.Parse(dr["Latitude"].ToString()), decimal.Parse(dr["Longitude"].ToString()));
            }

            ds.Tables.Add(dt);
            return ds;        
        }

        protected void RadMap1_ItemDataBound(object sender, Telerik.Web.UI.Map.MapItemDataBoundEventArgs e)
        {
            MapMarker marker = e.Item as MapMarker;
            if (marker != null)
            {
                DataRowView item = e.DataItem as DataRowView;
                string ServiceCenterName = item.Row["ServiceCenterName"] as string;
                string country = item.Row["Country"] as string;
                string city = item.Row["City"] as string;
                string address = item.Row["Address"] as string;
                string googleSearchServiceCenterName = "";
                if (ServiceCenterName.Contains("&")) 
                {
                    googleSearchServiceCenterName = ServiceCenterName.Replace("&", "%26"); 
                }
                else 
                {
                    googleSearchServiceCenterName = ServiceCenterName; 
                }
                marker.TooltipSettings.Content = String.Format(TOOLTIP_TEMPLATE, country.ToLower().Replace(" ", string.Empty), ServiceCenterName, city, address, googleSearchServiceCenterName);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string path = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E";
            string SQL;
            int iRowCnt;
            clsDBO clSC = new clsDBO();
            SQL = "select * from servicecenter " +
                "where zip = '" + txtZip.Text + "' " +
                "and not longitude is null ";
            clSC.OpenDB(SQL, path);
            iRowCnt = clSC.RowCount();
            if (iRowCnt > 0)
            {
                clSC.GetRow();
                RadMap1.DataSource = GetData2(clSC.GetFields("longitude"), clSC.GetFields("latitude"));
                RadMap1.DataBind();
                RadMap1.CenterSettings.Latitude = Convert.ToDouble(clSC.GetFields("latitude"));
                RadMap1.CenterSettings.Longitude = Convert.ToDouble(clSC.GetFields("longitude"));
                RadMap1.Zoom = 9;
            }

        }

        private DataSet GetData2(String xLong, string xLat)
        {
            string path = "server=198.143.98.122;database=veritas;User Id=sa;Password=NCC1701E";
            DataSet ds = new DataSet("ServiceCenterLocations");

            DataTable dt = new DataTable("ServiceCenterLocationsTable");
            dt.Columns.Add("Shape", Type.GetType("System.String"));
            dt.Columns.Add("Country", Type.GetType("System.String"));
            dt.Columns.Add("ServiceCenterName", Type.GetType("System.String"));
            dt.Columns.Add("City", Type.GetType("System.String"));
            dt.Columns.Add("Address", Type.GetType("System.String"));
            dt.Columns.Add("Latitude", Type.GetType("System.Decimal"));
            dt.Columns.Add("Longitude", Type.GetType("System.Decimal"));

            string sSQL = "select * from veritas.dbo.ServiceCenter where Latitude is not NULL AND Longitude is not NULL " +
                          "and servicecenterid in ( " +
                          "SELECT ServiceCenterID " +
                          "FROM ServiceCenter where SQRT(power(69.1 * (latitude - " + xLat +
                          "), 2) +POWer(69.1 * (" + xLong +
                          " - longitude) * COS(latitude / 57.3), 2)) < 50) ";
            CSharpDBO.CSharpDBO dBO = new CSharpDBO.CSharpDBO();
            dBO.dboOpen(path);
            DataTable currentServiceCenters = dBO.dboInportInformation(sSQL);
            dBO.dboClose();

            foreach (DataRow dr in currentServiceCenters.Rows)
            {
                dt.Rows.Add("customColorShape", "United States", dr["ServiceCenterName"].ToString(), dr["City"].ToString(), dr["Addr1"].ToString() + " " + dr["Zip"].ToString(), decimal.Parse(dr["Latitude"].ToString()), decimal.Parse(dr["Longitude"].ToString()));
            }

            ds.Tables.Add(dt);
            return ds;
        }

    }
}