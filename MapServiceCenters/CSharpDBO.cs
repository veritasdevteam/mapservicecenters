﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;

namespace CSharpDBO
{
    public class CSharpDBO
    {
        SqlConnection conn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter dataAdapter = new SqlDataAdapter();
        DataTable dataTable = new DataTable();


        public CSharpDBO() { }
        public void dboOpen(string path)
        {
            /*The purpose of this method is to open a connection to a database*/
            conn = new SqlConnection(path);
            conn.Open();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandTimeout = 6000;
        }
        public void dboClose()
        {
            /*The purpose of this method is to close a connection to a database*/
            conn.Close();
        }
        public DataTable dboInportInformation(string sqlCommand)
        {
            /*The purpose of this method is to be able to read out information from the database using a data table
             use a foreach loop in this format to read out the data
             use foreach (DataRow dataRow in DataTable.Rows) to read in data*/

            cmd.CommandText = sqlCommand;
            cmd.Connection = conn;
            dataAdapter = new SqlDataAdapter(cmd);
            dataTable = new DataTable();

            dataAdapter.Fill(dataTable);
            return dataTable;
        }
        public void dboAlterDBOUnsafe(string sqlCommand)
        {
            /*The purpose of this method is to execute queries. 
            Warning: Do not use this method if you are getting information from the user.
            Use AlterDBOSafe*/
            cmd.CommandText = sqlCommand;
            cmd.Connection = conn;
            cmd.ExecuteNonQuery();
        }
        public bool hasRows()
        {
            /*The Purpose of this method is to verify if there is information in a database after a query is executed. Make sure to use an Alter Method first*/
            bool hasRows = true;
            DataTableReader dataTableReader = new DataTableReader(dataTable);
            if (dataTableReader.HasRows)
            {
                hasRows = true;
            }
            else
            {
                hasRows = false;
            }


            return hasRows;
        }
        public void AlterDBOSafe(string[] QueryInformation, List<object> dataToBeRead, SqlDbType[] dataType, string sqlCommand)
        {
            /*The purpose of this method is to execute queries using user information safely.  
            values is the Query information. dataToBeRead is the data to be inserted into the Database
            dataType is the sql data type
            sqlCommand should be formated as such:
            insert into RedShieldAdminTest.dbo.DatabaseTest(Fname, Lname, address) values(@Fname, @Lanme, @address)*/

            cmd.CommandText = sqlCommand;
            SqlParameter[] parameter = new SqlParameter[QueryInformation.Length];

            for (int i = 0; i < QueryInformation.Length; i++)
            {
                parameter[i] = new SqlParameter();
                parameter[i].SqlDbType = dataType[i];
                parameter[i].ParameterName = QueryInformation[i];
                parameter[i].Value = dataToBeRead[i];

            }
            foreach (SqlParameter sqlParameter in parameter)
            {
                cmd.Parameters.Add(sqlParameter);
            }
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();

        }
        public DataTable dboInportInformationSafe(string[] QueryInformation, List<object> dataToBeRead, SqlDbType[] dataType, string sqlCommand)
        {

            cmd.Connection = conn;
            cmd.CommandText = sqlCommand;
            SqlParameter[] parameter = new SqlParameter[QueryInformation.Length];
            for (int i = 0; i < QueryInformation.Length; i++)
            {
                parameter[i] = new SqlParameter();
                parameter[i].SqlDbType = dataType[i];
                parameter[i].ParameterName = QueryInformation[i];
                parameter[i].Value = dataToBeRead[i];

            }
            foreach (SqlParameter sqlParameter in parameter)
            {
                cmd.Parameters.Add(sqlParameter);
            }
            cmd.CommandTimeout = 60000;
            dataAdapter = new SqlDataAdapter(cmd);
            dataTable = new DataTable();
            dataAdapter.Fill(dataTable);



            return dataTable;
        }
        public SqlDbType setSqlDbType(string sSQLType)
        {
            if (sSQLType.ToLower() == "bigint")
            {
                return SqlDbType.BigInt;
            }
            else if (sSQLType.ToLower() == "binary")
            {
                return SqlDbType.Binary;
            }
            else if (sSQLType.ToLower() == "bit")
            {
                return SqlDbType.Bit;
            }
            else if (sSQLType.ToLower() == "char")
            {
                return SqlDbType.Char;
            }
            else if (sSQLType.ToLower() == "date")
            {
                return SqlDbType.Date;
            }
            else if (sSQLType.ToLower() == "datetime")
            {
                return SqlDbType.DateTime;
            }
            else if (sSQLType.ToLower() == "datetime2")
            {
                return SqlDbType.DateTime2;
            }
            else if (sSQLType.ToLower() == "datetimeoffset")
            {
                return SqlDbType.DateTimeOffset;
            }
            else if (sSQLType.ToLower() == "decimal")
            {
                return SqlDbType.Decimal;
            }
            else if (sSQLType.ToLower() == "float")
            {
                return SqlDbType.Float;
            }
            else if (sSQLType.ToLower() == "image")
            {
                return SqlDbType.Image;
            }
            else if (sSQLType.ToLower() == "int")
            {
                return SqlDbType.Int;
            }
            else if (sSQLType.ToLower() == "money")
            {
                return SqlDbType.Money;
            }
            else if (sSQLType.ToLower() == "nchar")
            {
                return SqlDbType.NChar;
            }
            else if (sSQLType.ToLower() == "ntext")
            {
                return SqlDbType.NText;
            }
            else if (sSQLType.ToLower() == "nvarchar")
            {
                return SqlDbType.NVarChar;
            }
            else if (sSQLType.ToLower() == "real")
            {
                return SqlDbType.Real;
            }
            else if (sSQLType.ToLower() == "smalldatetime")
            {
                return SqlDbType.SmallDateTime;
            }
            else if (sSQLType.ToLower() == "smallint")
            {
                return SqlDbType.SmallInt;
            }
            else if (sSQLType.ToLower() == "smallmoney")
            {
                return SqlDbType.SmallMoney;
            }
            else if (sSQLType.ToLower() == "structured")
            {
                return SqlDbType.Structured;
            }
            else if (sSQLType.ToLower() == "text")
            {
                return SqlDbType.Text;
            }
            else if (sSQLType.ToLower() == "time")
            {
                return SqlDbType.Time;
            }
            else if (sSQLType.ToLower() == "timestamp")
            {
                return SqlDbType.Timestamp;
            }
            else if (sSQLType.ToLower() == "tinyint")
            {
                return SqlDbType.TinyInt;
            }
            else if (sSQLType.ToLower() == "udt")
            {
                return SqlDbType.Udt;
            }
            else if (sSQLType.ToLower() == "uniqueidentifier")
            {
                return SqlDbType.UniqueIdentifier;
            }
            else if (sSQLType.ToLower() == "varbinary")
            {
                return SqlDbType.VarBinary;
            }
            else if (sSQLType.ToLower() == "VarChar")
            {
                return SqlDbType.VarChar;
            }
            else if (sSQLType.ToLower() == "variant")
            {
                return SqlDbType.Variant;
            }
            else
            {
                return SqlDbType.Xml;
            }
        }

    }
}
