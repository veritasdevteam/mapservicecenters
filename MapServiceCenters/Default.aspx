﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MapServiceCenters.Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head runat="server">
    <title>Map Example</title>
    <link href="styles.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false"/>
        <asp:Table runat="server">
            <asp:TableRow>
                <asp:TableCell>
                    <h2 class="mapTitle">Veritas Service Centers</h2>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Table runat="server">
            <asp:TableRow>
                <asp:TableCell Font-Bold="true">
                    Zip Code:
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="txtZip" MaxLength="5" runat="server"></asp:TextBox>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click" BackColor="#1eabe2" runat="server" Text="Search" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <telerik:RadMap RenderMode="Lightweight" runat="server" ID="RadMap1" Zoom="3" CssClass="MyMap" OnItemDataBound="RadMap1_ItemDataBound" >
            <CenterSettings Latitude="45.95" Longitude="-97.97" />
            <DataBindings>
                <MarkerBinding DataShapeField="Shape" DataTitleField="City" DataLocationLatitudeField="Latitude" DataLocationLongitudeField="Longitude"/>
            </DataBindings>
            <LayersCollection>
                <telerik:MapLayer Type="Tile" Subdomains="a,b,c"
                    UrlTemplate="https://#= subdomain #.tile.openstreetmap.org/#= zoom #/#= x #/#= y #.png"
                    Attribution="&copy; <a href='http://osm.org/copyright' title='OpenStreetMap contributors' target='_blank'>OpenStreetMap contributors</a>.">
                </telerik:MapLayer>
            </LayersCollection>
        </telerik:RadMap>
    </form>
</body>
</html>